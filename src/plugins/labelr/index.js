import LabelrTable from "./table/Table.vue";

const LabelrUI = {
  install(Vue) {
    Vue.component(LabelrTable.name, LabelrTable);
  },
};

export default LabelrUI;
